require('dotenv').config();
const axios = require('axios');
const Discord = require('discord.js');
const { exit } = require('process');
const client = new Discord.Client({ intents: ["GUILDS", "GUILD_MESSAGES"] });
const fetchP = import('node-fetch').then(mod => mod.default)
const fetch = (...args) => fetchP.then(fn => fn(...args))
const API = require('./api.js');
const api = new API(); 
client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

/* DISCORD BOT - ARK NITRADO --- BY TERRYN RIVERA */
client.on('messageCreate', async msg => {
    switch (msg.content) {
        case "!arkbot status":
            if(msg.author.id !== msg.guild.ownerId) {
               return false;
            }
            let name = "server-status";
            let everyoneRole = msg.guild.roles.everyone;
            let setup = false, server_status = false;
            msg.guild.channels.cache.forEach(c => {
                if(c.name == name) {
                    setup = true;
                    server_status = c;
                }
            })
            if(!setup) {
                await msg.guild.channels.create('Cluster Information', {
                    type: 'GUILD_CATEGORY',
                    position: 1,
                    permissionOverwrites: [
                        {
                            id: msg.guild.id,
                            allow: ['VIEW_CHANNEL'],
                        }]
                }).then(async cat => {
                    await msg.guild.channels.create('server-status', {
                    type: 'GUILD_TEXT',
                    parent: cat.id,
                    permissionOverwrites: [
                        {
                            id: msg.guild.id,
                            allow: ['VIEW_CHANNEL'],
                        }]
                    }).then(c => {
                        setup = true;
                        server_status = c;
                    })

                })
                await msg.guild.channels.create('Display', {
                    type: 'GUILD_CATEGORY',
                    position: 1,
                    permissionOverwrites: [
                        {
                            id: msg.guild.id,
                            deny: ['VIEW_CHANNEL'],
                        }, 
                        {
                            id: msg.guild.me,
                            allow: 'VIEW_CHANNEL'
                            }]
                }).then(async cat => {
                    await msg.guild.channels.create('Players Online: 0', {
                    type: 'GUILD_VOICE',
                    parent: cat.id,
                    permissionOverwrites: [
                        {
                            id: msg.guild.id,
                            allow: ['VIEW_CHANNEL'],
                            deny: ['CONNECT']
                        }, 
                        {
                            id: msg.guild.me,
                            allow: ['VIEW_CHANNEL','CONNECT']
                        }]
                    })
                    await msg.guild.channels.create('Servers Online: 0', {
                        type: 'GUILD_VOICE',
                        parent: cat.id,
                        permissionOverwrites: [
                            {
                                id: msg.guild.id,
                                allow: ['VIEW_CHANNEL'],
                                deny: ['CONNECT'],
                            }, 
                            {
                                id: msg.guild.me,
                                allow: ['VIEW_CHANNEL','CONNECT']
                                }]
                        })
                })
            }
            
            const _allMessageEmbed = {
                solo: {
                    color: 0x0099ff,
                    title: 'Server Status',
                    url: process.env.BASE_URL,
                    fields: [
                    ],
                    timestamp: new Date(),
                    footer: {
                        text: 'Updated every 5 minutes'
                    },
                },
            };

            server_status.bulkDelete(5)
                .then(async message => {;
                    server_status.send({ embeds: [_allMessageEmbed.solo] })
                    .then(async (message) => {
                        await api.getServices()
                        .then(async services => {
                            let servers = [], all_servers = [];
                            services.data.forEach(element => {
                                servers.push(element);
                            })
                            for (const server of servers) {
                                let i = await api.getService(server.id);
                                let online_channel = false;
                                
                                if(i.data && i.success) {
                                    i.channel = false;
                                    let channel_cluster_information = false;
                                    msg.guild.channels.cache.forEach(c => {
                                        if(c.name == `${api.mapConvert(i.data.map).toLowerCase().replaceAll(' ','-')}-players-online-id${i.data.id}`) {
                                            i.channel = c;
                                        }
                                        if(c.name == "Cluster Information") {
                                            channel_cluster_information = c;
                                        }
                                    })
                                    if(!i.channel) {
                                        await msg.guild.channels.create(`${api.mapConvert(i.data.map).toLowerCase().replaceAll(' ','-')}-players-online-id${i.data.id}`, {
                                        type: 'GUILD_TEXT',
                                        parent: channel_cluster_information.id,
                                        permissionOverwrites: [
                                            {
                                                id: msg.guild.id,
                                                deny: ['VIEW_CHANNEL'],
                                            }, 
                                            {
                                                id: msg.guild.me,
                                                allow: 'VIEW_CHANNEL'
                                                }]
                                        }).then(c => {
                                            i.channel = c;
                                        })
                                    }
                                    let player_online_embed  = {};
                                    i.channel.bulkDelete(5)
                                    .then(async _po_msg => {
                                        player_online_embed = {
                                            color: 0x0099ff,
                                            title: api.mapConvert(i.data.map),
                                            url: 'https://thatothercluster.com',
                                            description:`**${i.data.players_online} `+(i.data.players_online == 1 ? "Player" : "Players" )+` Online**\nOnline Players are retrieved every 5 minutes.`,
                                            timestamp: new Date(),
                                            footer: {
                                                text: 'Retrieved',
                                            },
                                        };
                                        i.channel.send({ embeds: [player_online_embed] })
                                        .then(async (_po_msg) => {
                                            i.msg = _po_msg;
                                            player_online_embed.timestamp = new Date();
                                            let description = `**${i.data.players_online} `+(i.data.players_online == 1 ? "Player" : "Players" )+` Online**\nOnline Players are retrieved every 5 minutes.`
                                            
                                            i.data.allplayers.forEach(element => {
                                                if(element.online) {
                                                    var timeStart = new Date(element.last_online).getHours();
                                                    var timeEnd = new Date().getHours();  
                                                    description += `\n> ${element.name}`;
                                                }
                                            });
                                            if(!description) {
                                                description = `**No Players Online**\nOnline Players are retrieved every 5 minutes.`;
                                            }
                                            player_online_embed.description = description;
                                            _po_msg.edit({ embeds: [player_online_embed] })
                                        });
                                    });
                                }
                                all_servers.push({server: i, service: server});
                            }
                            
                            msg.guild.channels.cache.forEach(c => {
                                if(c.name) {
                                    if(c.name.indexOf('Players Online') !== -1) {
                                        let allUsersOnline = 0;
                                        all_servers.forEach(element => {
                                            if(element.server.data && element.server.success)
                                                allUsersOnline += element.server.data.players_online;
                                        });
                                        c.setName(`Players Online: ${allUsersOnline}`)
                                    }
                                    if(c.name.indexOf('Servers Online') !== -1) {
                                        let allServersOnline = 0;
                                        all_servers.forEach(element => {
                                            if(element.server.data && element.server.success&& element.server.data.status == "Online")
                                                allServersOnline++;
                                        });
                                        c.setName(`Servers Online: ${allServersOnline}`)
                                    }
                                }
                            });

                            message.edit({ embeds:api.setEmbedsServerStatus(all_servers) })     
                            setInterval(async function() {
                                let all_servers = [];
                                for (const server of servers) {
                                    let i = await api.getService(server.id);
                                    let description = "";
                                    all_servers.push({server: i, service: server});
                                    msg.guild.channels.cache.forEach(c => {
                                        if(c.name == `${api.mapConvert(i.data.map).toLowerCase().replaceAll(' ','-')}-players-online-id${i.data.id}`) {
                                            c.messages.fetch({ limit: 1 }).then(messages => {
                                                for( const message of messages) {
                                                    c.messages.fetch({around: message.id, limit: 1})
                                                    .then(msg => {
                                                        const fetchedMsg = msg.first();
                                                        let player_online_embed = {
                                                            color: 0x0099ff,
                                                            title: api.mapConvert(i.data.map),
                                                            url: process.env.BASE_URL,
                                                            description:`**${i.data.players_online} `+(i.data.players_online == 1 ? "Player" : "Players" )+` Online**\nOnline Players are retrieved every 5 minutes.`,
                                                            timestamp: new Date(),
                                                            footer: {
                                                                text: 'Retrieved',
                                                            },
                                                        };
                                                        i.data.allplayers.forEach(element => {
                                                            if(element.online) { 
                                                                description += `\n> ${element.name}`;
                                                            }
                                                        });
                                                        if(!description) {
                                                            description = `**No Players Online**\nOnline Players are retrieved every 5 minutes.`;
                                                        }
                                                        player_online_embed.description = description;
                                                        fetchedMsg.edit({ embeds: [player_online_embed] })
                                                    });
                                                    break;
                                                }
                                            })
                                        }
                                    })
                                }
                                msg.guild.channels.cache.forEach(c => {
                                    if(c.name) {
                                        if(c.name.indexOf('Players Online') !== -1) {
                                            let allUsersOnline = 0;
                                            all_servers.forEach(element => {
                                                if(element.server.data && element.server.success)
                                                    allUsersOnline += element.server.data.players_online;
                                            });
                                            c.setName(`Players Online: ${allUsersOnline}`)
                                        }
                                        if(c.name.indexOf('Servers Online') !== -1) {
                                            let allServersOnline = 0;
                                            all_servers.forEach(element => {
                                                if(element.server.data && element.server.success&& element.server.data.status == "Online")
                                                    allServersOnline++;
                                            });
                                            c.setName(`Servers Online: ${allServersOnline}`)
                                        }
                                    }
                                });
                                message.edit({ embeds: api.setEmbedsServerStatus(all_servers)}) 
                            }, 5 * (1000 * 60))
                        });
                    }
                ).catch(error => {
                    console.log(error);
                })
            });
        break;
     }
  })
client.login(process.env.CLIENT_TOKEN); //login bot using token