require('dotenv').config(); //initialize dotenv
const fetchP = import('node-fetch').then(mod => mod.default)
const fetch = (...args) => fetchP.then(fn => fn(...args))
const axios = require('axios');
const crypto = require('crypto');


class API {
    constructor() {
        this.url = "";
        this.headers = {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${process.env.NITRADO_ACCESS_TOKEN}`
        }
    }
    dateConvert(date,past) {
        let dateFuture, dateNow
        if(past) {
            dateFuture = new Date();
            dateNow = new Date(date);
        } else {
            dateFuture = new Date(date);
            dateNow = new Date();
        }
        let seconds = Math.floor((dateFuture - (dateNow))/1000);
        let minutes = Math.floor(seconds/60);
        let hours = Math.floor(minutes/60);
        let days = Math.floor(hours/24);

        hours = hours-(days*24);
        minutes = minutes-(days*24*60)-(hours*60);
        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60);
        let text = "";
        if(days) {
            if(days < 2) {
                text += days + " Day ";
            } else {
                text += days + " Days ";
            }
        }
        if(hours) {
            if(hours < 2) {
                text += hours + " Hour ";
            } else {
                text += hours + " Hours ";
            }
        }
        if(minutes) {
            if(minutes < 2) {
                text += minutes + " Minute ";
            } else {
                text += minutes + " Minutes ";
            }
        }
        return text;
    }
    mapConvert(map) {
        let name = "N/A";
        switch(map) {
            case "CrystalIsles":
                name = "Crystal Isles";
            break;
            case "TheIsland":
                name = "The Island";
            break;
            case "Genesis":
                name = "Genesis";
            break;
            case "Aberration_P":
                name = "Aberration";
            break;
            case "Gen2":
                name = "Gen2";
            break;
            case "Extinction":
                name = "Extinction";
            break;
            case "Valguero_P":
                name = "Valguero";
            break;
            case "LostIsland":
                name = "Lost Island";
            break;
            case "CrystalIsles":
                name = "Crystal Isles";
            break;
            case "TheCenter":
                name = "The Center";
            break;
            case "ScorchedEarth_P":
                name = "Scorched Earth";
            break;
            case "Ragnarok":
                name = "Ragnarok";
            break;
        }  
        return name;
    }
    async getServices() {
        let url = "https://api.nitrado.net/services";
        let data = await fetch(url, {
            method: 'GET',
            headers: this.headers,
        })
        .then( response => response.text())
        .then (text => {
            let data = JSON.parse(text), services = [];
            if(data) {
                if(data.status == "success"){
                    data.data.services.forEach(element => {
                        services.push({
                        id: element.id,
                        status: element.status,
                        slots: element.type_human,
                        name: element.details.name,
                        runtime: element.suspend_date
                        })
                    });
                    return new Promise(function(resolve, reject) {
                        resolve({success: true, message: "Data retrieved successfully!", data: services});
                    });
                } else {
                    return new Promise(function(resolve, reject) {
                        resolve({success: false, message: "Error getting services"});
                    });
                }
            } else {
                return new Promise(function(resolve, reject) {
                    resolve({success: false, message: "Error getting services"});
                });
            }
        });
        return data;
    }
    setEmbedsServerStatus(all_servers) {
        let _count = 0;
        let _sendEmbeds = [
            {
                color: 0x0099ff,
                title: 'Server Status',
                url: "https://thatothercluster.com",
                fields: [
                ]
            }
        ];
        let embeds = Math.ceil((all_servers.length) / 3), embeds_on = 0, embeds_allowed = 3;
        all_servers.forEach(el => {
            if(embeds_on == embeds_allowed) {
                _count++;
                _sendEmbeds[_count] = {
                    color: 0x0099ff,
                    fields: [
                    ],
                };
                embeds_on = 1;
                _sendEmbeds[_count].fields = [];
                
            } else {
                embeds_on++;
            }
            if(el.server.success) {
                let element = el.server.data;
                _sendEmbeds[_count].fields.push({
                    name: 'Session',
                    value: (el.service.name.includes('Gameserver') ? element.name : el.service.name) ,
                    inline: false,
                });
                _sendEmbeds[_count].fields.push({
                    name: 'Map',
                    value: (element.status == "Online" ? '<:green_circle:961620254998552586>' : (element.status == "Restarting" ? '<:orange_circle:964915640488034334>' : '<:red_circle:964915591909613588>'))+this.mapConvert(element.map),
                    inline: true,
                });
                _sendEmbeds[_count].fields.push({
                    name: 'Players',
                    value: `${element.players_online}/${element.slots}`,
                    inline: true,
                });
                _sendEmbeds[_count].fields.push({
                    name: 'Mode',
                    value: (element.pvp ? "PVP" : "PVE"),
                    inline: true,
                });
                _sendEmbeds[_count].fields.push({
                    name: 'Runtime',
                    value: this.dateConvert(el.service.runtime),
                    inline: true,
                });
                _sendEmbeds[_count].fields.push({ name: '\u200B', value: '\u200B', inline: true });
            } else {
                _sendEmbeds[_count].fields.push({
                    name: "Error",
                    value: el.server.message
                });
            }     
        });
        _sendEmbeds[_count].timestamp = new Date();
        _sendEmbeds[_count].footer = {
            text: 'Updated every 5 minutes'
        }
        return _sendEmbeds;
    }
    async getService(id) {
        let server = {};
        let url = `https://api.nitrado.net/services/${id}/gameservers`;
        
        let __data = await fetch(url, {
            method: 'GET',
            headers: this.headers,
        })
        .then( response => response.text())
        .then (async text => {
            let data = JSON.parse(text);
            if(data) {
              if(data.status == "success"){
                  let el = data.data.gameserver;
                  let map =  el.settings.config.map.split(',');

                    let name = el.query.server_name;
                    if(name) {
                        name = name.replaceAll(`\\`, '');
                    } else {
                        name = "Gameserver";
                    }
                    server = {
                      id: el.service_id,
                      status: (el.status == "started" ? "Online" : (el.status == 'restarting' ? "Restarting" : "Offline")),
                      map: map[2],
                      name: name,
                      slots: el.slots,
                      position: map[1],
                      hash: crypto.createHash('md5').update(el.settings.general.clusterid).digest('hex'),
                      pvp: (el.settings.config['disable-pvp'] == 'false' ? true : false)
                  }
                  url = `https://api.nitrado.net/services/${id}/gameservers/games/players`;
                  let __data = await fetch(url, {
                    method: 'GET',
                    headers: this.headers,
                })
                .then( response => response.text())
                .then (text => {
                    try {
                        let data = JSON.parse(text);
                        if(data) {
                        if(data.status == "success"){
                            let online = 0, total = 0;
                            data.data.players.forEach(element => {
                                if(element.online) {
                                    online++;
                                }
                                total++;
                            });
                            server.allplayers = data.data.players;
                            server.players = total;
                            server.players_online = online;
                            return new Promise(function(resolve, reject) {
                                resolve({success: true, message: "Data retrieved successfully!", data: server});
                            });
                        } else {
                            return new Promise(function(resolve, reject) {
                                resolve({success: false, message: "Error getting services"});
                            });
                        }
                        } else {
                            return new Promise(function(resolve, reject) {
                                resolve({success: false, message: "Error getting services"});
                            });
                        }
                    } catch(error) {
                        return new Promise(function(resolve, reject) {
                            resolve({success: false, message: "Error getting services", error: error});
                        });
                    }
                })
                return __data;
              } else {
                return new Promise(function(resolve, reject) {
                    resolve({success: false, message: "Error getting services"});
                });
              }

            } else {
                return new Promise(function(resolve, reject) {
                    resolve({success: false, message: "Error getting services"});
                });
            }
        });
        return __data;
    }
}
module.exports = API;